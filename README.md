## OpenCV

### Programas

* Anaconda: <https://www.anaconda.com/>


### Librerías
* OpenCV: <https://opencv.org/>
* PIP:<https://pypi.org/project/pip/>

### Instalación usando pip (Linux y Python 3.x.x)
* pip install opencv-python
* pip install opencv-contrib-python

